# Calculadora

Ejercicio basico en python para resolver las operaciones basicas ingresando 2 numeros y la operacion a efectuar.

Veremos la resolucion del problema desde diferentes enfoques o paradigmas de programacion

## Ejecucion

para ejecutar el programa solo se necesita python v3 comprobar con :

```bash
  $ python -V
```

luego ejecutar el archivo 

```bash
 $ python calculadora.py
```

